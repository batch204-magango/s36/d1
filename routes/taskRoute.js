//Contains all the endpoints for our application
//We seperate the routes such that "index.js file" only contains information on the server

const express = require("express");
//Express router 
const router = express.Router();

const taskController = require("../controllers/taskController")

//Create Routes for application

//Route to get all the tasks

router.get("/", (req, res)=>{

	//Invokes the getAllTask function from the taskcontroller.js file and sends the result back to the client postman

	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
});
//add
//Create another route
//create task
router.post("/", (req, res) => {
	console.log(req.body)
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
});

//delete 
router.delete("/:id", (req, res)=> {
	console.log(req.params)

	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

//Route to update a task

router.put('/:id', (req, res)=> {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})



//Use "module.exports" to export the router object to use in the "index.js"
module.exports = router;
